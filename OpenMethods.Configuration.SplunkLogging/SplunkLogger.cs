﻿using System;
using System.IO;
using System.Net;
using System.Threading.Tasks;

using Newtonsoft.Json.Linq;
using OpenMethods.Configuration.Interfaces;

namespace OpenMethods.Configuration.SplunkLogging
{
    public class SplunkLogger : ILogger
    {
        private readonly string _splunkUrl;
        private readonly string _splunkToken;
        private string _prodOrDev;

        public SplunkLogger()
        {
            try
            {

                _splunkUrl = Environment.GetEnvironmentVariable("SPLUNK_URL");
                _splunkToken = Environment.GetEnvironmentVariable("SPLUNK_TOKEN");
                _prodOrDev = "DEV"; // Environment.GetEnvironmentVariable("PROD");

                if (string.IsNullOrEmpty(_splunkUrl))
                {
                    _splunkUrl = "https://http-inputs-openmethods.splunkcloud.com/services/collector/event";
                }
                if (string.IsNullOrEmpty(_splunkToken))
                {
                    _splunkToken = "ECEC365E-B7B0-4DB0-A341-CB697E92C41D";
                }

                if (string.IsNullOrEmpty(_splunkUrl))
                {
                    _splunkUrl = "https://http-inputs-openmethods.splunkcloud.com/services/collector/event";
                }
                //}

            }
            catch (Exception)
            {
                // ignored
            }

        }
        public void SendLog(string message)
        {
            throw new NotImplementedException();
        }



        /// <summary>
        /// 
        /// </summary>
        /// <param name="eventData"></param>
        /// <param name="host"></param>
        /// <param name="source"></param>
        /// <param name="sourceType"></param>
        public void SendLog(JObject eventData, string host, string source, string sourceType)
        {
            Task.Run(() =>
            {
                try
                {
                    //JObject data = new JObject(
                    //    new JProperty("controller", "AgentConnect"),
                    //    new JProperty("method", "ctr"),
                    //    new JProperty("AK", _accessKey),
                    //    new JProperty("SK", _secretKey)
                    //);
                    //SplunkLog log = new SplunkLog();
                    //log.SendLogsToSplunk(data, "ConnectAPI");

                    if (_splunkUrl != null)
                    {
                        _prodOrDev = "ConfigAPI-" + _prodOrDev;
                        SendLogsToSplunk(eventData, host, _prodOrDev, "_json");
                    }
                }
                catch (Exception)
                {
                    // ignored
                }
            });
        }

        private void SendLogsToSplunk(JObject eventData, string host, string source, string sourceType)
        {
            try
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(_splunkUrl);
                request.Method = "POST";
                request.ContentType = "application/json";
                request.Headers.Add(HttpRequestHeader.Authorization, "Splunk " + _splunkToken);

                TimeSpan t = DateTime.UtcNow - new DateTime(1970, 1, 1);
                // int secondsSinceEpoch = (int) t.TotalSeconds;
                double secondsSinceEpoch = t.TotalMilliseconds;

                if (string.IsNullOrEmpty(host))
                {
                    host = "Unknown";
                }

                JObject data = new JObject(
                    new JProperty("event", eventData),
                    new JProperty("source", source), // _prodOrDev
                    new JProperty("sourcetype", sourceType), //  "_json"
                    new JProperty("host", host),
                    new JProperty("time", secondsSinceEpoch)
                );

                using (var streamWriter = new StreamWriter(request.GetRequestStream()))
                {
                    streamWriter.Write(data);
                    streamWriter.Flush();
                    streamWriter.Close();
                }

                WebResponse response = request.GetResponse();

                HttpStatusCode statusCode = ((HttpWebResponse)response).StatusCode;

                if (statusCode == HttpStatusCode.OK)
                {
                    Stream stream = response.GetResponseStream();
                    if (stream != null)
                    {
                        StreamReader streamReader = new StreamReader(stream);
                        string responseContent = streamReader.ReadToEnd();

                        streamReader.Close();
                        stream.Close();
                    }

                    response.Close();
                }
            }
            catch (Exception exception)
            {
                string g = exception.ToString();
                // ignored
            }
        }
    }
}
