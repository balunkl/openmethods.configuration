﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net;
using System.Threading.Tasks;

using Amazon;
using Amazon.DynamoDBv2;
using Amazon.DynamoDBv2.DocumentModel;
using Amazon.DynamoDBv2.Model;

using OpenMethods.Configuration.Models;
using OpenMethods.Configuration.Models.Enums;
using OpenMethods.Configuration.Models.CrmInstances;

using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Linq;

namespace OpenMethods.Configuration.DynamoRepository
{
    public class DbCommon
    {
        AmazonDynamoDBClient _client;

        public DbCommon()
        {
            DbClient client = new DbClient();
            _client = client.getDynamoClient();

        }

        public async Task<IList<CrmTypes>> GetCrmTypes()
        {
            List<CrmTypes> crmTypes = new List<CrmTypes>();

            var req = new ExecuteStatementRequest();

            string miscName = "CrmTypes";

            req.Statement = "SELECT * FROM MiscData WHERE  \"miscName\" = \'" + miscName + "\'";// WHERE 'OrgId' = \'" + id + "\'";

            req.ConsistentRead = true;

            ExecuteStatementResponse result = await _client.ExecuteStatementAsync(req);

            List<Dictionary<string, AttributeValue>> items = result.Items;

            HttpStatusCode httpStatusCode = result.HttpStatusCode;
            

            foreach (Dictionary<string, AttributeValue> item in items)
            {
                
                List<AttributeValue> list = item["versions"].L;

                List<string> versions = new List<string>();//.AddRange(from k in list select k.S);

                versions.AddRange(from k in list select k.S);

                crmTypes.Add(new CrmTypes
                {
                    name = item["name"].S,
                    versions = versions,
                    type = item["type"].S
                });
            }

            return crmTypes;
        }

        public async Task<IList<CtiTypes>> GetCtiTypes()
        {
            List<CtiTypes> ctiTypes = new List<CtiTypes>();

            var req = new ExecuteStatementRequest();

            string miscName = "CtiTypes";

            req.Statement = "SELECT * FROM MiscData WHERE  \"miscName\" = \'" + miscName + "\'";

            req.ConsistentRead = true;

            ExecuteStatementResponse result = await _client.ExecuteStatementAsync(req);

            List<Dictionary<string, AttributeValue>> items = result.Items;

            HttpStatusCode httpStatusCode = result.HttpStatusCode;


            foreach (Dictionary<string, AttributeValue> item in items)
            {

                List<AttributeValue> list = item["versions"].L;

                List<string> versions = new List<string>();

                versions.AddRange(from k in list select k.S);

                ctiTypes.Add(new CtiTypes
                {
                    name = item["name"].S,
                    versions = versions,
                    type = item["type"].S
                });
            }

            return ctiTypes;
        }


    }
}
