﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Threading.Tasks;
using Amazon;
using Amazon.DynamoDBv2;
using Amazon.DynamoDBv2.DocumentModel;
using Amazon.DynamoDBv2.Model;

using Amazon.Runtime;

using System.Net;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace OpenMethods.Configuration.DynamoRepository
{
    public class DB_Test_Client
    {

        private static AmazonDynamoDBClient _dynamoDbClient;
        private readonly string _tableName;

        public DB_Test_Client()
        {
            //private IConfigurationSystem Configuration;

           // NameValueCollection appConfig = ConfigurationManager.AppSettings;
            string accessKey = "AKIAJZEVQDTQJ6QSXVCA";// appConfig["ACCESS_KEY"];
            string secretKey = "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";// appConfig["SECRET_KEY"];
            _tableName = "CS_Customers";// appConfig["INTERACTION_TABLE"];

            RegionEndpoint regionEndPoint = RegionEndpoint.USWest1;

            AWSCredentials credentials = new BasicAWSCredentials(accessKey, secretKey);
            _dynamoDbClient = new AmazonDynamoDBClient(credentials, regionEndPoint);
        }

        public AmazonDynamoDBClient getDbClient()
        {
            return _dynamoDbClient;
        }

        public static string CreateTestItem(string message, int? totalInsert)
        {
           

            string id = Guid.NewGuid().ToString();

            try
            {
  


                var watch = new System.Diagnostics.Stopwatch();

               

                var watch2 = new System.Diagnostics.Stopwatch();

                watch.Start();

                int totalIns = 0;

                if (totalInsert == null)
                    totalIns = 1;

                insertRecords(id, totalIns);

               // watch.Stop();

                var writeTime = watch.ElapsedMilliseconds;

                watch.Restart();

                int count = 0;
                string nextToken = "";
                long readTime = 0;
                int i = 0;

                List<Setting> settings = new List<Setting>();

                while (nextToken != null)
                {
                    i += 1;

                    AgentRecords agentRecords = getData(nextToken, 0);

                    nextToken = agentRecords.nextToken;

                    count += agentRecords.TotalRecords;
                    readTime += agentRecords.TimeElapsed;

                    settings.AddRange(agentRecords.Settings);

                   // if (i == 4)
                     //   nextToken = null;
                }

                var serialized = JsonConvert.SerializeObject(settings);
               // string serialized = serialized.Replace(@"""", "'");

                // var batch = getDataBatch("");

                // var result = Task.Run(async () => await getDataBatch()).Result;

                // readTime = watch.ElapsedMilliseconds;

                watch.Stop();

                watch2.Start();

                

               // var results = a.Items.c.map(AWS.DynamoDB.Converter.unmarshall)

              //  string jsonoutput = ConvertIntoJson(a);

               // string jsonoutput1 = jsonoutput.Replace(@"""", "'");

                
                watch2.Stop();

                var convertTime = watch2.ElapsedMilliseconds;



                return id + " : " + settings.Count +  " Total Records: " + count + " Read Time: " + readTime + " Write Time: " + writeTime + " Convert Time: " + convertTime;
            }
            catch (Exception exception)//
            {
                throw new Exception(exception.Message);
            }
        }

        private string addCrmUser(int count)
        {
            count = 1;

            for (int i = 0; i < count; i++)
            {


                string id1 = Guid.NewGuid().ToString();



                Setting s = new Setting
                {
                    OrgId = id1,
                    DeploymentId = "bala",
                    PropBool = true,
                    PropInt = 100,
                    PropString = "OnAnswer",
                    SubSet = new SubSetting
                    {
                        Url = "https://openmethods.com/",
                        Port = 12312
                    },
                    PropArray = new string[] { "Lunch", "Break", "After Call Work", "Traininng", "Lunch", "Break", "After Call Work", "Traininng" , "Lunch", "Break", "After Call Work", "Traininng",
                    "Lunch", "Break", "After Call Work", "Traininng","Lunch", "Break", "After Call Work", "Traininng" ,"Lunch", "Break", "After Call Work", "Traininng" ,"Lunch", "Break", "After Call Work", "Traininng"
                    }

                };

                string so1 = JsonConvert.SerializeObject(s);

                string so = so1.Replace(@"""", "'");

                var req = new ExecuteStatementRequest();

                req.Statement = "INSERT INTO CS_Customers VALUE " + so;

                Task<ExecuteStatementResponse> res = _dynamoDbClient.ExecuteStatementAsync(req);
                res.Wait(10000);
            }


            return "";
        }

        private static async Task getDataBatch(string nextToken)
        {
            var watch1 = new System.Diagnostics.Stopwatch();

            int totalRecords = 0;

            watch1.Start();

            var req = new BatchExecuteStatementRequest();

            string id = "5af625b4-a5c7-43de-916d-b26719429175";
            string deploymentId = "bala"; 

            BatchStatementRequest batchStatementRequest = new BatchStatementRequest();


            batchStatementRequest.Statement = @"SELECT * FROM CS_Customers";// WHERE 'OrgId' = \'" + id + "\' AND 'DeploymentId' = 'bala'";

           // batchStatementRequest.Statement = "SELECT * FROM CS_Customers WHERE \"OrgId\" = \'" + id + "\' AND \"DeploymentId\" = \'" + deploymentId + "\'";// WHERE 'OrgId' = \'" + id + "\'";

          //  batchStatementRequest.Statement = "SELECT * FROM \"CS_Customers\".\"DeploymentId-index\" WHERE \"DeploymentId\" = \'" + deploymentId + "\'";// WHERE 'OrgId' = \'" + id + "\'";

            batchStatementRequest.ConsistentRead = true;

            //batchStatementRequest.NextToken = nextToken;

            List<BatchStatementRequest> list = new List<BatchStatementRequest>();
            list.Add(batchStatementRequest);

            req.Statements = list; // new List<BatchStatementRequest>();
          //  { batchStatementRequest };



            // req.ReturnConsumedCapacity.//


            //BatchExecuteStatementResponse a = Task.Run(async () =>
            //await _dynamoDbClient.BatchExecuteStatementAsync(req)
            //).Result;



            Task<BatchExecuteStatementResponse> res2 = _dynamoDbClient.BatchExecuteStatementAsync(req);
            res2.Wait(30000);

           // Task.Delay(10000).Wait(1000);


            BatchExecuteStatementResponse a = res2.Result;

           // ConsumedCapacity cap = a.ConsumedCapacity;
            HttpStatusCode httpStatusCode = a.HttpStatusCode;
           // string nextToken1 = a.NextToken;
            string requestId = a.ResponseMetadata.RequestId;
            var metaDAta = a.ResponseMetadata.Metadata;
            long contentLength = a.ContentLength;

            List<BatchStatementResponse> ress2 = a.Responses;

            BatchStatementResponse res12;

            foreach (var item in ress2)
            {
                res12 = item;
                totalRecords = item.Item.Count;
            }
           // totalRecords = ress[0].Item.

            // var c = new Converter<a.Items, obejct>();

            // List<Setting> list = a.Items.ConvertAll<Setting>(new Converter<Dictionary<string, AttributeValue>, Setting>(convert));


           // string pr = a.Items[0]["PropString"].S;

           // bool prb = a.Items[0]["PropBool"].BOOL;
           // string pro = a.Items[0]["OrgId"].S;

          //  Dictionary<string, AttributeValue> map1 = a.Items[0]["SubSet"].M;


            //foreach (var item in a.Items)
            //{

            //    // var dict = a.Items.ToArray(x => x.Key, x => GetValueFromAttribute(x.Value));



            //    foreach (string keystr in item.Keys)
            //    {


            //        // AttributeValue
            //    }
            //}


            var readTime = watch1.ElapsedMilliseconds;

            watch1.Stop();

          //  return totalRecords;
        }


        private static AgentRecords getData(string nextToken, int totalStart)
        {
            var watch1 = new System.Diagnostics.Stopwatch();

            int totalRecords = 0;

            watch1.Start();

            var req = new ExecuteStatementRequest();
         

            string id = "5af625b4-a5c7-43de-916d-b26719429175";

            string deploymentId = "bala";

           // req.Statement = "SELECT * FROM CS_Customers WHERE \"OrgId\" = \'" + id + "\' AND \"DeploymentId\" = \'" + deploymentId + "\'";// WHERE 'OrgId' = \'" + id + "\'";

            req.Statement = "SELECT * FROM CS_Customers WHERE  \"DeploymentId\" = \'" + deploymentId + "\'";// WHERE 'OrgId' = \'" + id + "\'";

            req.ConsistentRead = true;

            if (nextToken != null && nextToken != "")
                req.NextToken = nextToken;

            Task<ExecuteStatementResponse> res2 = _dynamoDbClient.ExecuteStatementAsync(req);
            res2.Wait(10000);


            ExecuteStatementResponse a = res2.Result;

            ConsumedCapacity cap = a.ConsumedCapacity;
            HttpStatusCode httpStatusCode = a.HttpStatusCode;
            string nextToken1 = a.NextToken;
            string requestId = a.ResponseMetadata.RequestId;
            var metaDAta = a.ResponseMetadata.Metadata;
            long contentLength = a.ContentLength;

             totalRecords = a.Items.Count;

            totalRecords = totalRecords + totalStart;

            // var c = new Converter<a.Items, obejct>();

            // List<Setting> list = a.Items.ConvertAll<Setting>(new Converter<Dictionary<string, AttributeValue>, Setting>(convert));




            // List<String> l12 = (AttributeValue)propArray;
            List<Setting> settings = new List<Setting>();

            foreach (var item in a.Items)
            {

                // var dict = a.Items.ToArray(x => x.Key, x => GetValueFromAttribute(x.Value));

               // string pr8 = a.Items[0]["PropString"].S;

                string pr = item.ContainsKey("PropString") ? item["PropString"].S : "";

                string depId = item["DeploymentId"].S;

             //   string pr = item["PropString"].S;

                bool prb = item.ContainsKey("PropBool") ? item["PropBool"].BOOL : false;  

                string orgId = item["OrgId"].S;

                Dictionary<string, AttributeValue> map1 = item.ContainsKey("SubSet") ? item["SubSet"].M : null; 
                System.Collections.IList propArray = item.ContainsKey("PropArray") ? item["PropArray"].L : null;

                //ExpectedAttributeValue ex = new ExpectedAttributeValue();

                //List<String> columnInfoStrings = map1.ConvertAll(x => JsonSerializer.Serialize(x, options));
                //List<Row> rows = response.Rows;


                Setting setting = new Setting();
                setting.DeploymentId = depId;
                setting.OrgId = orgId;
                setting.PropString = pr;
                // setting.PropArray = (AttributeValue)propArray;

                //setting.SubSet = map1;

                settings.Add(setting);
            }

            //  Amazon.Runtime.Internal.Transform.StringUnmarshaller.GetInstance().Unmarshall(;







            var readTime = watch1.ElapsedMilliseconds;

            AgentRecords agentRecords = new AgentRecords();
            agentRecords.TotalRecords = totalRecords;
            agentRecords.TimeElapsed = readTime;
            agentRecords.Settings = settings;
            agentRecords.nextToken = nextToken1;

            watch1.Stop();

            if (nextToken1 != null)
            {
             // totalRecords +=  getData(nextToken1, totalRecords);
            }

            return agentRecords;
        }

        private static Setting convert(Dictionary<string, AttributeValue> df)
        {

            //var dict = df.ToDictionary(x => x.Key, x => GetValueFromAttribute(x.Value));


            // DynamoDBEntry dentry =new DynamoDBEntry(;

            //DynamoDBEntryConversion dyc =  DynamoDBEntryConversion.V2.ConvertFromEntry<Setting>(new DynamoDBEntry() {
            //})
            //    ;


            Setting s = new Setting();

            //foreach (string keystr in df.Keys)
            //{
            //  //

            //    if (keystr.Trim() == "") continue;
            //    strVal = null;
            //    //        if (keystr.Trim() == "SubSet") x = 0;
            //    df.TryGetValue(keystr, out strVal);
            //    returnvalue += "\"" + keystr + "\":";

            //  //  string str = ItemIoJson(strVal);

            //    returnvalue += str;




            //    if (ks != df.Keys.Count) returnvalue += ",";
            //}

            return s;

        }

        private static void insertRecords(string id, int count)
        {
            for (int i = 0; i < count; i++)
            {


                string id1 = Guid.NewGuid().ToString();

                if (count == 1)
                    id1 = id;

                Setting s = new Setting
                {
                    OrgId = id1,
                    DeploymentId = "bala",
                    PropBool = true,
                    PropInt = 100,
                    PropString = "OnAnswer",
                    SubSet = new SubSetting
                    {
                        Url = "https://openmethods.com/",
                        Port = 12312
                    },
                    PropArray = new string[] { "Lunch", "Break", "After Call Work", "Traininng", "Lunch", "Break", "After Call Work", "Traininng" , "Lunch", "Break", "After Call Work", "Traininng",
                    "Lunch", "Break", "After Call Work", "Traininng","Lunch", "Break", "After Call Work", "Traininng" ,"Lunch", "Break", "After Call Work", "Traininng" ,"Lunch", "Break", "After Call Work", "Traininng" 
                    }

                };

                string so1 = JsonConvert.SerializeObject(s);

                string so = so1.Replace(@"""", "'");

                var req = new ExecuteStatementRequest();

                req.Statement = "INSERT INTO CS_Customers VALUE " + so;

                Task<ExecuteStatementResponse> res = _dynamoDbClient.ExecuteStatementAsync(req);
                res.Wait(10000);
            }

            //req.Statement = "INSERT INTO CS_Customers VALUE {'OrgId': \'" + id1 + "\','DeploymentId':'bala', 'UserLimit':1000, "+ so + "}";

        }

        private void BatchInsert()
        {

            var req_batch = new BatchExecuteStatementRequest();

            BatchStatementRequest b_s = new BatchStatementRequest();

            List<BatchStatementRequest> b_s_list = new List<BatchStatementRequest> { };
            b_s_list.Add(b_s);


            req_batch.Statements = b_s_list;

            Task<BatchExecuteStatementResponse> res_batch = _dynamoDbClient.BatchExecuteStatementAsync(req_batch);
        }


        private static string ConvertIntoJson(ExecuteStatementResponse a)
        {
            string returnvalue = "[";
            AttributeValue strVal = null;
            int itms = 0;
            foreach (var item in a.Items)
            {
                //  var x = 0;
                itms++;
                var df = item;
                returnvalue += "{";
                int ks = 0;
                foreach (string keystr in df.Keys)
                {
                    ks++;

                    if (keystr.Trim() == "") continue;
                    strVal = null;
                    //        if (keystr.Trim() == "SubSet") x = 0;
                    df.TryGetValue(keystr, out strVal);
                    returnvalue += "\"" + keystr + "\":";

                    string str = ItemIoJson(strVal);

                    returnvalue += str;




                    if (ks != df.Keys.Count) returnvalue += ",";
                }

                returnvalue += "}";
                if (itms != a.Items.Count) returnvalue += ",";
            }
            returnvalue += "]";
            return returnvalue;
        }

        private static string ItemIoJson(AttributeValue strVal)
        {
            string returnvalue = "";


          

            if (strVal.SS.Count > 0)
            {
                returnvalue += "[";
                System.Collections.IList list = strVal.SS;
                for (int i = 0; i < list.Count; i++)
                {


                    returnvalue += "\"" + list[i] + "\"";

                    if (i != list.Count - 1) returnvalue += ",";
                }
                returnvalue += "]";

            }
            else if (strVal.BS.Count > 0)
            {
                returnvalue += "[";
                System.Collections.IList list = strVal.BS;
                for (int i = 0; i < list.Count; i++)
                {
                    AttributeValue str = (AttributeValue)list[i];
                    returnvalue += ItemIoJson(str);
                    if (i != list.Count - 1) returnvalue += ",";
                }
                returnvalue += "]";
            }
            else if (strVal.NS.Count > 0)
            {
                returnvalue += "[";
                System.Collections.IList list = strVal.NS;
                for (int i = 0; i < list.Count; i++)
                {
                    AttributeValue str = (AttributeValue)list[i];
                    returnvalue += ItemIoJson(str);
                    if (i != list.Count - 1) returnvalue += ",";
                }
                returnvalue += "]";

            }
            else if (strVal.L.Count > 0)
            {
                returnvalue += "[";
                System.Collections.IList list = strVal.L;
                for (int i = 0; i < list.Count; i++)
                {
                    AttributeValue str = (AttributeValue)list[i];
                    returnvalue += ItemIoJson(str);
                    if (i != list.Count - 1) returnvalue += ",";
                }
                returnvalue += "]";

            }
            else if (strVal.M.Count > 0)
            {
                int ks = 0;
                AttributeValue strV = null;
                returnvalue += "{";
                foreach (string keyval in strVal.M.Keys)
                {
                    ks++;

                    if (keyval.Trim() == "") continue;

                    strVal.M.TryGetValue(keyval, out strV);
                    returnvalue += "\"" + keyval + "\":";

                    string str = ItemIoJson(strV);

                    returnvalue += str;

                    if (ks != strVal.M.Keys.Count) returnvalue += ",";
                }
                returnvalue += "}";


            }
            else if (strVal.S != null)
                returnvalue += "\"" + strVal.S.ToString() + "\"";
            else if (strVal.N != null)
                returnvalue += strVal.N.ToString();
            else if (strVal.BOOL != null)
                returnvalue += "\"" + strVal.BOOL.ToString() + "\"";
            else
                returnvalue += "\"\"";

            if (returnvalue.Trim() == "") returnvalue += "\"\"";

            return returnvalue;
        }



        private static string replaceCharsInString(string value)
        {
            return value.Replace(@"\", "");
        }


    }

    class CrmUser
    {
        public string UserId { get; set; }
        public string InstanceId { get; set; }
        public string Name  { get; set; }
    }
    

    class AgentRecords
    {
        public int TotalRecords { get; set; }
        public string nextToken { get; set; }
        public List<Setting> Settings { get; set; }

        public long TimeElapsed { get; set; }
    }

    class Setting
    {
        public string PropString { get; set; }
        public int PropInt { get; set; }

        public bool PropBool { get; set; }

        public string OrgId { get; set; }

        public string DeploymentId { get; set; }

        public SubSetting SubSet { get; set; }

        public string[] PropArray { get; set; }
    }

    class SubSetting
    {
        public string Url { get; set; }
        public int Port { get; set; }
    }
}


//foreach (var item in a.Items)
//{
//    var df = item;

//    Console.Write(df);



//}

// Table table = Table.LoadTable(_dynamoDbClient, "CS_Customers");

// Task<Document> obj =  table.GetItemAsync("d1e99b98-450c-4885-9865-ec568a9cf9b9","bala");

//var dd = obj.Result.ToJson();



// foreach (var item in a.Items)
// {
//     var df = item;

//    // Console.Write(df);

//    var dg = JsonConvert.SerializeObject(df);

//     var ddd = dg;

// }



//req.Statement = "DELETE FROM CS_Customers where OrgId = '1' AND DeploymentId = '2'";
//Task<ExecuteStatementResponse> res1 = _dynamoDbClient.ExecuteStatementAsync(req);
//res1.Wait(10000);

//int count = a.Items.Count;

//var request = new PutItemRequest
//{
//    TableName = "CS_Customers",
//    Item = new Dictionary<string, AttributeValue>()
//    {
//        { "OrgId", new AttributeValue {
//              S = id
//          }},
//        { "DeploymentId", new AttributeValue {
//              S = message
//          }},
//        { "DateReceived", new AttributeValue {
//            S = DateTime.UtcNow.ToString("MM/dd/yyyy hh:mm:ss.fffff tt")
//          }}
//        //,
//        //{ "MapData", new AttributeValue {
//        //      M = new Dictionary<string, AttributeValue> {
//        //    {"InteractionId", new AttributeValue("Asdasdas")}},
//        //  }}
//    }
//};

