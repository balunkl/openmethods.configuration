﻿using System;
using System.Collections.Generic;
using OpenMethods.Configuration.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Amazon;
using Amazon.DynamoDBv2;
using Amazon.DynamoDBv2.DocumentModel;
using Amazon.DynamoDBv2.Model;

using Amazon.Runtime;
using System.Threading.Tasks;

using System.Net;

namespace OpenMethods.Configuration.DynamoRepository
{
    public class DbCustomer : DbBase
    {
        AmazonDynamoDBClient _client;

        public DbCustomer()
        {
            DbClient client = new DbClient();
            _client = client.getDynamoClient();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public async Task<HttpStatusCode> Create(string name)
        {
            try
            {
                string orgId = Guid.NewGuid().ToString();

                Customer customer = new Customer
                {
                    id = orgId,
                    name = name,
                    userLimit = -1,
                    isActive = true

                };

                string jsonCustomer = JsonConvert.SerializeObject(customer);

                jsonCustomer = jsonCustomer.Replace(@"""", "'");

                var req = new ExecuteStatementRequest
                {
                    Statement = "INSERT INTO Customers VALUE " + jsonCustomer
                };

                ExecuteStatementResponse result = await _client.ExecuteStatementAsync(req);

                return result.HttpStatusCode;
            }
            catch (Exception exception)
            {

                throw new Exception(exception.Message);
            }

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public async Task<HttpStatusCode> Update(string id, string name)
        {
            try
            {

                var req = new ExecuteStatementRequest
                {
                    Statement = "UPDATE Customers SET \"name\" = \'" + name + "\' WHERE  \"id\" = \'" + id + "\'"  // AND \"name\" = \'" + name + "\'"
                };

                ExecuteStatementResponse result = await _client.ExecuteStatementAsync(req);

                return result.HttpStatusCode;
            }
            catch (Exception exception)
            {

                throw new Exception(exception.Message);
            }

        }

        public async Task<IList<Customer>> GetCustomers()
        {
            List<Customer> customers = new List<Customer>();

            var req = new ExecuteStatementRequest();

            req.Statement = "SELECT * FROM Customers";// WHERE  \"DeploymentId\" = \'" + deploymentId + "\'";// WHERE 'OrgId' = \'" + id + "\'";

            req.ConsistentRead = true;

           ExecuteStatementResponse result = await _client.ExecuteStatementAsync(req);

           List<Dictionary<string,AttributeValue>> items = result.Items;

            HttpStatusCode httpStatusCode = result.HttpStatusCode;
            int totalRecords = items.Count;

            foreach (Dictionary<string,AttributeValue> item in items)
            {
                string customerId = item["id"].S;
                string customerName = item["name"].S;
                string userLimit = item["userLimit"].N;
                bool isActive = item.ContainsKey("isActive") ? item["isActive"].BOOL : true;

                customers.Add(new Customer
                {
                    id = customerId,
                    name = customerName,
                    userLimit = Convert.ToInt32(userLimit),
                    isActive = isActive
                });
            }

            return customers;
        }

        public async Task<Customer> GetCustomer(string id)
        {
            Customer customer = new Customer();

            var req = new ExecuteStatementRequest();

            req.Statement = "SELECT * FROM Customers  WHERE  \"id\" = \'" + id + "\'";

            req.ConsistentRead = true;

            ExecuteStatementResponse result = await _client.ExecuteStatementAsync(req);

           // ExecuteStatementResponse result = response.;

            List<Dictionary<string, AttributeValue>> items = result.Items;

            HttpStatusCode httpStatusCode = result.HttpStatusCode;
            int totalRecords = items.Count;

            foreach (Dictionary<string, AttributeValue> item in items)
            {
                customer.id = item["id"].S;
                customer.name = item["name"].S;
                customer.userLimit = Convert.ToInt32(item["userLimit"].N);
                customer.isActive = item.ContainsKey("isActive") ? item["isActive"].BOOL : true;

            }

            return customer;
        }

        public async Task<HttpStatusCode> Delete(string id)
        {

           //Customer customer = await this.GetCustomer(id);

            var req = new ExecuteStatementRequest();

            req.Statement = "DELETE FROM Customers WHERE \"id\" = \'" + id + "\'"; ;// AND \"name\" = \'" + customer.name + "\'";// WHERE 'OrgId' = \'" + id + "\'";

            ExecuteStatementResponse result = await _client.ExecuteStatementAsync(req);

           // ExecuteStatementResponse result = response.Result;

            HttpStatusCode httpStatusCode = result.HttpStatusCode;


            return httpStatusCode;
        }

    }
}
