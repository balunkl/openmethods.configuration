﻿using System;
using System.Collections.Generic;
using System.Text;

using Amazon;
using Amazon.DynamoDBv2;
using Amazon.DynamoDBv2.DocumentModel;
using Amazon.DynamoDBv2.Model;

namespace OpenMethods.Configuration.DynamoRepository
{
    public class DbBase
    {
        AmazonDynamoDBClient _client;

        public DbBase()
        {
            DbClient client = new DbClient();
            _client = client.getDynamoClient();
        }

        public AmazonDynamoDBClient GetDbClient()
        {
            return _client;
        }
    }
}
