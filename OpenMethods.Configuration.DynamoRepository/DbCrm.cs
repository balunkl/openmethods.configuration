﻿using Amazon.DynamoDBv2;
using Amazon.DynamoDBv2.Model;
using Newtonsoft.Json;
using OpenMethods.Configuration.Models.Enums;
using OpenMethods.Configuration.Models.CrmInstances;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Linq;

namespace OpenMethods.Configuration.DynamoRepository
{
    public class DbCrm
    {
        AmazonDynamoDBClient _client;

        public DbCrm()
        {
            DbClient client = new DbClient();
            _client = client.getDynamoClient();

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public async Task<HttpStatusCode> Create(string name, string nativeId, string deploymentId, string instanceType, string typeName, string version, string customerId)
        {
            try
            {
                string id = Guid.NewGuid().ToString();


                List<IExtension> extensions = CrmInstance.GetExtensions(instanceType);

                CrmInstance crmInstance = new CrmInstance
                {
                    deploymentId = deploymentId,
                    id = id,
                    name = name,
                    nativeId = nativeId,
                    type = instanceType,
                    typeName = typeName,
                    version = version,
                    extensions = extensions
                };

             

                string json = JsonConvert.SerializeObject(crmInstance);

                json = json.Replace(@"""", "'");

                var executeStatementRequest = new ExecuteStatementRequest
                {
                    Statement = "INSERT INTO CrmInstances VALUE " + json
                };

                ExecuteStatementResponse result = await _client.ExecuteStatementAsync(executeStatementRequest);

                return result.HttpStatusCode;
            }
            catch (Exception exception)
            {

                throw new Exception(exception.Message);
            }

        }

        public async Task<IList<CrmInstance>> GetCrmInstances(string deploymentId)
        {
            List<CrmInstance> crmInstances = new List<CrmInstance>();

            var req = new ExecuteStatementRequest();

            req.Statement = "SELECT * FROM CrmInstances WHERE \"deploymentId\" = \'" + deploymentId + "\'";// WHERE 'OrgId' = \'" + id + "\'";

            req.ConsistentRead = true;

            ExecuteStatementResponse result = await _client.ExecuteStatementAsync(req);

            List<Dictionary<string, AttributeValue>> items = result.Items;

            HttpStatusCode httpStatusCode = result.HttpStatusCode;
            int totalRecords = items.Count;

            foreach (Dictionary<string, AttributeValue> item in items)
            {
                List<IExtension> extensions = new List<IExtension>();

                List<AttributeValue> list = item["extensions"].L;

                PopFlowSettings pop = new PopFlowSettings();

                foreach (AttributeValue ex in list)
                {
                    Dictionary<string, AttributeValue> m = ex.M;
                    pop.name = m["name"].S;
                    pop.screenPopType = m.ContainsKey("screenPopType") ? m["screenPopType"].S : nameof(ScreenPopType.OnAnswer);
                }

                extensions.Add(pop);

                crmInstances.Add(new CrmInstance
                {
                    id = item["id"].S,
                    name = item["name"].S,
                    nativeId = item["nativeId"].S,
                    type = item["type"].S,
                    typeName = item["typeName"].S,
                    version = item["version"].S,
                    extensions = extensions
                });
            }

            return crmInstances;
        }

        public async Task<CrmInstance> GetCrmInstance(string deploymentId, string crmInstanceId)
        {
            CrmInstance crmInstance = null;

            var req = new ExecuteStatementRequest
            {
                Statement = "SELECT * FROM CrmInstances WHERE \"id\" = \'" + crmInstanceId + "\' AND \"deploymentId\" = \'" + deploymentId + "\'",

                ConsistentRead = true
            };

            ExecuteStatementResponse result = await _client.ExecuteStatementAsync(req);

            List<Dictionary<string, AttributeValue>> items = result.Items;

            HttpStatusCode httpStatusCode = result.HttpStatusCode;
            int totalRecords = items.Count;

            foreach (Dictionary<string, AttributeValue> item in items)
            {

                List<IExtension> extensions = new List<IExtension>();

                List<AttributeValue> list = item["extensions"].L;

                PopFlowSettings pop = new PopFlowSettings();

                foreach (AttributeValue ex in list)
                {
                   Dictionary<string, AttributeValue> m = ex.M;
                    pop.name =  m["name"].S;
                    pop.screenPopType = m.ContainsKey("screenPopType") ? m["screenPopType"].S : nameof(ScreenPopType.OnAnswer); 
                } 

                extensions.Add(pop);

                crmInstance = new CrmInstance
                {
                    id = item["id"].S,
                    name = item["name"].S,
                    nativeId = item["nativeId"].S,
                    type = item["type"].S,
                    typeName = item["typeName"].S,
                    version = item["version"].S
                };

                crmInstance.extensions = extensions;
            }

            return crmInstance;
        }


        public async Task<HttpStatusCode> Delete(string deploymentId, string crmInstanceId)
        {

            var req = new ExecuteStatementRequest();

            req.Statement = "DELETE FROM CrmInstances WHERE \"id\" = \'" + crmInstanceId + "\' AND \"deploymentId\" = \'" + deploymentId + "\'";

            ExecuteStatementResponse result = await _client.ExecuteStatementAsync(req);

            HttpStatusCode httpStatusCode = result.HttpStatusCode;

            return httpStatusCode;
        }

    }
}
