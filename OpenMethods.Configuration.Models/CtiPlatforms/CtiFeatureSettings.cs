﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OpenMethods.Configuration.Models.CtiPlatforms
{
    public class CtiFeatureSettings
    {
        public bool UseCtiBridgeView { get; set; }

        public bool EnableCallerId { get; set; }

        public bool EnableCountryCodes { get; set; }

        public bool EnableDisposition { get; set; }

        public bool EnableLogoutReasons { get; set; }

        public bool AgentSelectEditable { get; set; }

        public bool ForceAgentLogoutOnExit { get; set; }

        public bool EnableExternalQuickDial { get; set; }

        public bool DisableSSO { get; set; }

        public bool DisableAgentUpdate { get; set; }

        public bool AutoActivateChannels { get; set; }

        public bool DisableExternalNotReadyReasons { get; set; }

        public bool InteractionDataEditable { get; set; }

    }
}
