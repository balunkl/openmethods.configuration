﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OpenMethods.Configuration.Models.CtiPlatforms
{
    public class CtiAgent: CtiBase
    {

        public string CtiPlatformId { get; set; }

        public string ParentId { get; set; }

        public string ParentHeirarchy { get; set; }

        public string UserName { get; set; }

        public string Password { get; set; }

        public string FullName { get; set; }

        public CtiFeatureSettings CtiFeatureSettings { get; set; }

        public AgentChannelSettings AgentChannelSettings { get; set; }
    }
}
