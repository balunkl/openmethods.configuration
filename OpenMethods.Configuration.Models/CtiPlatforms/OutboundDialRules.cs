﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OpenMethods.Configuration.Models.CtiPlatforms
{
    public class OutboundDialRules
    {

        public bool RemoveLeadingPlus { get; set; }


        public bool RemmoveLeadingOne { get; set; }


        public bool RemoveSpecialCharacters { get; set; }

        public short MaxLangth { get; set; }

        public string DialPrefix { get; set; }

        public string InternationalDialPrefix { get; set; }
    }

}
