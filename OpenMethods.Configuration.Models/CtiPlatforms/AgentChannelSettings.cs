﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OpenMethods.Configuration.Models.CtiPlatforms
{
    public class AgentChannelSettings
    {
        public bool ClientSideAutoAnswerCalls { get; set; } = false;

        public bool ClientSideAutoAnswerEmails { get; set; } = false;

        public bool ClientSideAutoAnswerChats { get; set; } = false;

        public bool EndCompletesInteraction { get; set; } = false;


        public string ChannelName { get; set; }

        public bool EnableEmail { get; set; } = false;

        public bool EnableChat { get; set; } = false;

        public bool EnableCallbacks { get; set; } = false;

        public bool EnableVoicemail { get; set; } = false;

    }
}
