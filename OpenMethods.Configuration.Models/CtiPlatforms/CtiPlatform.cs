﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OpenMethods.Configuration.Models.CtiPlatforms
{
    public class CtiPlatform : CtiBase
    {
        public string Id { get; set; }

        public string Name { get; set; }

        public Enums.CtiPlatformTypes PlatfornType { get; set; }

        public string Version { get; set; }

        public CtiFeatureSettings CtiFeatureSettings { get; set; }

        public AgentChannelSettings AgentChannelSettings { get; set; }
    }

}
