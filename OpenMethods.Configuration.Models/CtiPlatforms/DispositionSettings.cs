﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OpenMethods.Configuration.Models.CtiPlatforms
{
    public class DispositionSettings
    {

        public bool IsRequired { get; set; }

        public bool IsEditable { get; set; }

        public short MaxLength { get; set; } = 20;


    }
}
