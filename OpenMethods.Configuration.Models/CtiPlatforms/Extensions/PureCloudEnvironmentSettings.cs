﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OpenMethods.Configuration.Models.CtiPlatforms.Extensions
{
    public class PureCloudEnvironmentSettings
    {

        public string PureCloudHost { get; set; }

        public string PureCloudPort { get; set; }

        public bool EnableAutoDisposition { get; set; }

        public string AutoDispositionId { get; set; }


    }
}
