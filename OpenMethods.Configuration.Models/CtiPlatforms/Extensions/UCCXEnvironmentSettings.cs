﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OpenMethods.Configuration.Models.CtiPlatforms.Extensions
{
    public class UCCXEnvironmentSettings
    {

        public string CtiServerAddress { get; set; }

        public int CtiServerPort { get; set; } = 12028;

        public bool EnableFinesseAgentStatus { get; set; } = false;

        public string FinesseUri { get; set; }

        public int ReconnecrTimeout { get; set; } = 20000;

        public int RingNoAnswerTimeout { get; set; } = 30000;

        public bool AutoRetrieveCalls { get; set; } = true;

        public string ContetVariable1 { get; set; } = "CallVariable9";

        public string ContextVariable2 { get; set; } = "CallVariable10";
    }
}
