﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OpenMethods.Configuration.Models.CtiPlatforms.Extensions
{
   public class GenesysEnvironmentSettings
    {
        public string PrimaryConfigAddress { get; set; }

        public int PrimaryConfigPort { get; set; } = 2020;

        public string SecondaryConfigAddress { get; set; }

        public int SecondayConfigPort { get; set; } = 2020;

        public string EmailMediaType { get; set; }

        public string ChatMediaType { get; set; }



    }
}
