﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OpenMethods.Configuration.Models.CtiPlatforms.Extensions
{
    public class OAuthSettings
    {
        public string PrimaryGatewayIUrl { get; set; }

        public string PrimaryRedirectUrl { get; set; }

        public string SecondaryGatewayUrl { get; set; }

        public string SecondaryRedirectUrl { get; set; }

        public string AuthorizationUrl { get; set; }

        public string TokenUrl { get; set; }

        public string ClientId { get; set; }

        public string ClientSecret { get; set; }
    }
}
