﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OpenMethods.Configuration.Models.CtiPlatforms.Extensions
{
    public class AvayaEnvironmentSettings
    {

        public string PrimaryAESAddress { get; set; }

        public string PrimaryAESPort { get; set; }

        public string BackupAESAddress { get; set; }

        public string BackupAESPort { get; set; }

        public string TLinkName { get; set; }

        public string AESUsername { get; set; }

        public string AESPassword { get; set; }

        public string AESPollingTime { get; set; }

        public string UUIFormat { get; set; }

        public string UUIFixedFormat { get; set; }

        public bool AutoRetrieveEnabled { get; set; }

    }
}
