﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OpenMethods.Configuration.Models.CtiPlatforms.Extensions
{
    public class UCCXAgentReportSettings
    {

        public bool ShowAgentStatusReport { get; set; } = false;

        public bool ShowQueueStatsReport { get; set; } = false;

        public int QueueStatsReportInternal { get; set; } = 15000;

        public bool ShowCallJourneyReport { get; set; } = false;

        public Enums.ScreenPopType PopCallJourneyReport { get; set; } = Enums.ScreenPopType.OnAnswer;
    }
}
