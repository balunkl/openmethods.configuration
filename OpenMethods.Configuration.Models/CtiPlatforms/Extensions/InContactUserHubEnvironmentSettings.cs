﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OpenMethods.Configuration.Models.CtiPlatforms.Extensions
{
    public class InContactUserHubEnvironmentSettings
    {
        public string InContactDomain { get; set; }

        public string AccessKeyId { get; set; }


        public string AccessKeySecret  { get; set; }

    }
}
