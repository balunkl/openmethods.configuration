﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OpenMethods.Configuration.Models.CtiPlatforms.Extensions
{
    public class CICStationSettings
    {
        public Enums.CICStationTypes StationType { get; set; } = Enums.CICStationTypes.Workstation;

        public string StationName { get; set; }

        public string RemoteNumber { get; set; }

        public bool EnableWorkGroupDial { get; set; } = false;

        public bool EnableWorkGroupQuickDial { get; set; } = false;
    }
}
