﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OpenMethods.Configuration.Models.CtiPlatforms.Extensions
{
    public class InContactCentralEnvironmentSettings
    {
        public string ApplicationName { get; set; }


        public string VendorName { get; set; }

        public string BusinessUnitNumber
        {
            get; set;
        }
    }
}
