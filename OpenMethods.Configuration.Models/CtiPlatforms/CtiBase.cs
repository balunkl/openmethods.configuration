﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OpenMethods.Configuration.Models.CtiPlatforms
{
    public class CtiBase
    {

        public string OrgId { get; set; }
        public string DeploymentId { get; set; }
    }
}
