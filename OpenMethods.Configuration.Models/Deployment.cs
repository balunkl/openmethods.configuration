﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OpenMethods.Configuration.Models
{
    public class Deployment
    {

        public string Id { get; set; }

        public string Name { get; set; }

    }
}
