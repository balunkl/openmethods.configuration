﻿using System;
using System.Collections.Generic;

namespace OpenMethods.Configuration.Models
{
    public class Customer
    {
        public string id { get; set; }

        public string name { get; set; }

        public bool isActive { get; set; } = true;

        public int userLimit { get; set; }

        public IList<Deployment> deployments { get; set; }

    }
}
