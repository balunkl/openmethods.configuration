﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OpenMethods.Configuration.Models.CrmInstances
{
    public class CrmTypes
    {

        public string miscName { get; set; }

        public string name { get; set; }

        public string type { get; set; }

        public List<string> versions  { get; set; }
    }


    public class Version
    {
        public string version { get; set; }
    }


}
