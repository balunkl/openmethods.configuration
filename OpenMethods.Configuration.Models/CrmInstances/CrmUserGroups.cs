﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OpenMethods.Configuration.Models.CrmInstances
{
    public class CrmUserGroups : CrmBase
    {
        public string GroupId { get; set; }

        public string GroupName { get; set; }

        public string ParentId { get; set; }

        public string ParentHeirarchy { get; set; }

    }
}
