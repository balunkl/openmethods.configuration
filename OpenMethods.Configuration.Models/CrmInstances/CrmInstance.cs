﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OpenMethods.Configuration.Models.CrmInstances
{
    public class CrmInstance : CrmBase
    {
      
        public string id { get; set; }

        public string name { get; set; }

        public string nativeId { get; set; }

        public string type { get; set; }

        public string typeName { get; set; }

        public string version { get; set; }

        public List<IExtension> extensions { get; set; }

        public static List<IExtension> GetExtensions(string type)
        {
            List<IExtension> extensions = new List<IExtension>();

            if (type == "com.openmethods.ep.config.model.crm.servicecloud")
            {
                extensions.Add(new PopFlowSettings());
                extensions.Add(new CrmFeatureSettings());
                extensions.Add(new ServiceCloudConnectivitySettings());
            }

            return extensions;
        }

    }


    public class CrmFeatureSettings : IExtension
    {
        public string name { get; set; } = "crm.feature.settings";

        public bool enableInteractionTable { get; set; } = false;

    }


    public class Extension : IExtension
    {
        public string name { get; set; } = "servicecloud.user.popflow.settings";

        public PopFlowSettings data { get; set; }

        public int inheritedData { get; set; }
    }

    public interface IExtension {


        public string name { get; set; }
    }




}
