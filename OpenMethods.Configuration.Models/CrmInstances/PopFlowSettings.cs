﻿using System;
using System.Collections.Generic;
using System.Text;
 


namespace OpenMethods.Configuration.Models.CrmInstances
{
    public class PopFlowSettings : IExtension
    {
        public string name { get; set; } = "servicecloud.user.popflow.settings";

        public string screenPopType { get; set; } = nameof(Enums.ScreenPopType.OnAnswer);
    }
}
