﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OpenMethods.Configuration.Models.CrmInstances
{
    public class CrmUser : CrmBase
    {

        public string CrmInstanceId { get; set; }

        public string ParentId { get; set; }

        public string ParentHeirarchy { get; set; }

        public string UserName { get; set; }

        public string CrmId  { get; set; }

        public string FullName { get; set; }

        public PopFlowSettings PopFlowSettinga { get; set; }

        public List<CtiAgentMapping> CtiAgentMappings { get; set; }
    }

    public class CtiAgentMapping
    {
        public string CtiPlatformId { get; set; }

        public string CtiAgentId { get; set; }

        public string CtiAgentName { get; set; }

        public string GroupName { get; set; }
    }

}
