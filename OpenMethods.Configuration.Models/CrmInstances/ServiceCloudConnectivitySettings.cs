﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OpenMethods.Configuration.Models.CrmInstances
{
    public class ServiceCloudConnectivitySettings : IExtension
    {
        public string name { get; set; } = "servicecloud.connectivity.settings";

        public string siteUrl { get; set; }

        public string SiteUrl { get; set; }

        public string SiteName { get; set; }

        public string SoapEndpoint { get; set; }

        public string SoapUsername { get; set; }

        public string SoapPassword { get; set; }

        public string RestEndpoint { get; set; }
    }
}
