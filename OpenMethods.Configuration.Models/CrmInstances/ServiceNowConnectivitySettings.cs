﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OpenMethods.Configuration.Models.CrmInstances
{
    public class ServiceNowConnectivitySettings
    {

        public string SiteUrl { get; set; }

        public string SiteName { get; set; }
       
        public string RestEndpoint { get; set; }
    }
}
