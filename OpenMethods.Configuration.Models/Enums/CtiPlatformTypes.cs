﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OpenMethods.Configuration.Models.Enums
{
    public enum CtiPlatformTypes
    {
        UCCX = 1,
        UCCE = 2,
        GenesysEngage = 3,
        Avaya = 4,
        GenesysPureConnect = 5,
        GenesysPureCloud = 6,
        InContactCentral = 7,
        InContactUserHub = 8,
        Five = 9,
        Enghouse = 10,
        ProcessorBrider = 11
    }
}
