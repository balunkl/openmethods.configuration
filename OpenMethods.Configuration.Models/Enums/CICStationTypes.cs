﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OpenMethods.Configuration.Models.Enums
{
    public enum CICStationTypes
    {
        Workstation = 1,
        RemoteWorkstation = 2,
        RemoteNumber = 3,
        Stationless = 4
    }
}
