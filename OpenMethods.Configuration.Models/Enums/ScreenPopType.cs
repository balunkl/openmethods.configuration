﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OpenMethods.Configuration.Models.Enums
{
    public enum ScreenPopType
    {
        OnAnswer = 1,
        OnRing = 2
    }
    
}
