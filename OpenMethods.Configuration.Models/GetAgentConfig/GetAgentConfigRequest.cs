﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OpenMethods.Configuration.Models.GetAgentConfig
{
    public class GetAgentConfigRequest
    {

        public string customerId { get; set; }

        public string crmUsername { get; set; }
        public string crmNativeId { get; set; }

        public string ctiEnvironmentId { get; set; }

        public string ctiAgentId { get; set; }

    }

    
}
