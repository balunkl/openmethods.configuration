﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OpenMethods.Configuration.Models.GetAgentConfig
{
    public class GetAgentConfigResponseSuccess
    {

        public string result { get; set; } = "success";

        public string customerId { get; set; }

        public string customerName { get; set; }

        public string[] extensions { get; set; }

        public CrmInstance crmInstance { get; set; }

        public CtiEnvironment ctiEnvironment { get; set; }
    }

    public class CrmInstance
    {

        public string crmInstanceId { get; set; }

        public string crmNativeId { get; set; }

        public string crmInstanceName { get; set; }

        public string crmInstanceType { get; set; }

        public string crmInstanceVersion { get; set; }

        public List<Extensions> extensions { get; set; }

        public CrmUser crmUser { get; set; }
    }

    public class CrmUser
    {

        public string crmUserId { get; set; }

        public string crmUserCRMId { get; set; }

        public string crmUserUsername { get; set; }

        public string crmUserFullName { get; set; }

        public List<Extensions> extensions { get; set; }


    }


    public class CtiEnvironment
    {

        public string ctiEnvironmentId { get; set; }

        public string ctiEnvironmentName { get; set; }

        public string ctiEnvironmentType { get; set; }

        public string ctiEnvironmentVersion { get; set; }

        public CtiAgent ctiAgent { get; set; }

        public List<Extensions> extensions { get; set; }

        public List<QueueAdapters> queueAdapters { get; set; }

        public List<InteractionProcessors> interactionProcessors { get; set; }
    }


    public class CtiAgent
    {
        public string ctiAgentId { get; set; }

        public string ctiAgentUsername { get; set; }

        public string ctiAgentPassword { get; set; }

        public string ctiAgentFullName { get; set; }

        public List<Extensions> extensions { get; set; }

        public List<ReasonCodes> reasonCodes { get; set; }

        public List<InteractionDispositions> interactionDispositions { get; set; }

        public List<LogoutReasons> logoutReasons { get; set; }

        public List<CallerIdEntries> callerIdEntries { get; set; }

        public List<QuickDialEntries> quickDialEntries { get; set; }

    }

    public class Extensions
    {

    }

    public class QueueAdapters
    {

    }

    public class InteractionProcessors
    {

    }
    public class ReasonCodes
    {

    }

    public class InteractionDispositions
    {

    }

    public class LogoutReasons
    {

    }
    public class CallerIdEntries
    {

    }
    public class QuickDialEntries
    {

    }

}
