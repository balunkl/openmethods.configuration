﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OpenMethods.Configuration.Models.GetAgentConfig
{
    public class GetAgentConfigResponsePartial
    {

        public string result { get; set; } = "partial";

        public string customerId { get; set; }

        public string crmUsername { get; set; }

        public string crmNativeId { get; set; }

        public string crmInstanceId { get; set; }

        public CtiEnvironmentOptions ctiEnvironmentOptions { get; set; }
    }

    public class CtiEnvironmentOptions
    {
        public string ctiEnvironmentId { get; set; }

        public string ctiEnvironmentName { get; set; }

        public string ctiEnvironmentType { get; set; }

        public CtiAgentOptions ctiAgentOptions { get; set; }
    }


    public class CtiAgentOptions
    {
        public string ctiAgentId { get; set; }
        public string ctiAgentUsername { get; set; }
    }

}
