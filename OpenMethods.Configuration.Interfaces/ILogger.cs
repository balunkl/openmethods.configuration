﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;


namespace OpenMethods.Configuration.Interfaces
{
    public interface ILogger
    {

        public void SendLog(string message);

        public void SendLog(JObject eventData, string host, string source, string sourceType);

    }
}
